# Star System Creator

## What's this for?

Creating random planets for a roleplaying game.

I had originally intended to have it create entire star systems, but I found a better tool for that purpose so development stopped here.

## Project Goals

* Flex my REST service muscles
* Learn some JPA stuff
* Refresh my React skills 
* Generate random planets for a spacefaring RPG

## Running the application

1. Clone the code somewhere
2. In that directory:
```
./gradlew build
./gradlew bootRun
```
3. While bootRun is running, navigate to http://127.0.0.1:8080

### Caveat

* The application is configured to use h2 for database stuff.  Any planets you create will be lost when the application is closed.
* The first build of the application takes a *long* time.  I blame npm.

## Future work

There is probably not going to be much future development on this project.  I have found other tools that meet my needs, so I can focus my energies elsewhere.

If I were going to pick this up again:
1. The REST service could use more logging.
2. The system as a whole needs more test cases.
3. A system to display a list/hex grid of planets
4. An endpoint to create an entire stars system of planets (what I originally set out to do)
