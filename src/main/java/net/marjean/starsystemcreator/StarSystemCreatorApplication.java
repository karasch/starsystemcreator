package net.marjean.starsystemcreator;

import lombok.extern.java.Log;
import net.marjean.starsystemcreator.model.AtmosphereType;
import net.marjean.starsystemcreator.model.Planet;
import net.marjean.starsystemcreator.repository.*;
import net.marjean.starsystemcreator.service.PlanetCreator;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Log
public class StarSystemCreatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarSystemCreatorApplication.class, args);
	}

}
