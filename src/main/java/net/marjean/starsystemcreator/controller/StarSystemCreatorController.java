package net.marjean.starsystemcreator.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.marjean.starsystemcreator.model.*;
import net.marjean.starsystemcreator.repository.*;
import net.marjean.starsystemcreator.service.PlanetCreator;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
//@CrossOrigin(origins = "http://localhost:3000")
@AllArgsConstructor
@Slf4j
public class StarSystemCreatorController {

    private final AtmosphereTypeRepository atmosphereTypeRepository;
    private final PlanetRepository planetRepository;
    private final PlanetCreator planetCreator;
    private final SizeTypeRepository sizeTypeRepository;
    private final TemperatureTypeRepository temperatureTypeRepository;
    private final HydrographicsTypeRepository hydrographicsTypeRepository;
    private final PopulationTypeRepository populationTypeRepository;
    private final GovernmentTypeRepository governmentTypeRepository;
    private final FactionTypeRepository factionTypeRepository;
    private final StarPortTypeRepository starPortTypeRepository;
    private final TechLevelTypeRepository techLevelTypeRepository;
    private final TravelCodeTypeRepository travelZoneTypeRepository;

    @RequestMapping(path="/planet/{id}")
    public Planet getPlanet(@PathVariable("id") Integer id){
        return planetRepository.findById(id).get();
    }

    //@RequestMapping("/atmo")
    public AtmosphereType getAtmo() {
        // Remember that findById returns an Optional<AtmosphereType>
        return atmosphereTypeRepository.findById(1).get();
    }

    @RequestMapping("/random")
    public Planet getRandomPlanet(){
        Planet p = planetCreator.createRandomPlanet();
        planetRepository.save(p);

        return p;
    }

    @PostMapping("/update")
    public Planet updatePlanet(@RequestBody Planet input){

        if(input == null){
            log.warn("input was null!");
        }
        else {
            log.info("Msg received was: {}", input);

            //Need to set the planet value on the factions?
            for(Faction f : input.getFactions()){
                f.setPlanet(input);
            }
            //ditto for Star Port
            input.getStarPort().setPlanet(input);

            // Recalculate the trade codes
            Set<TradeCodeType> tradeCodes = planetCreator.getTradeCodeTypes(input);
            input.setTradeCodes(tradeCodes);

            planetRepository.save(input);
        }
        return input;
    }

    @RequestMapping("/list")
    public Iterable<Planet> listPlanets(){
        return planetRepository.findAll();
    }

    @RequestMapping("/sizes")
    public Iterable<SizeType> listSizes() { return sizeTypeRepository.findAll(); }

    @RequestMapping("/atmospheres")
    public Iterable<AtmosphereType> listAtmospheres(){ return atmosphereTypeRepository.findAll(); }

    @RequestMapping("/temperatures")
    public Iterable<TemperatureType> listTemperatures(){ return temperatureTypeRepository.findAll(); }

    @RequestMapping("/hydrographics")
    public Iterable<HydrographicsType> listHydrographics(){ return hydrographicsTypeRepository.findAll(); }

    @RequestMapping("/populations")
    public Iterable<PopulationType> listPopulations(){ return populationTypeRepository.findAll(); }

    @RequestMapping("/governments")
    public Iterable<GovernmentType> listGovernments(){ return governmentTypeRepository.findAll(); }

    @RequestMapping("/factionstrengths")
    public Iterable<FactionType> listFactionStrengths(){ return factionTypeRepository.findAll(); }

    @RequestMapping("/starporttypes")
    public Iterable<StarPortType> listStarPortTypes(){ return starPortTypeRepository.findAll(); }

    @RequestMapping("/techlevels")
    public Iterable<TechLevelType> listTechLevels(){ return techLevelTypeRepository.findAll(); }

    @RequestMapping("/travelzones")
    public Iterable<TravelZoneType> listTravelZones(){ return travelZoneTypeRepository.findAll(); }
}

