package net.marjean.starsystemcreator.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class AtmosphereType {

    public AtmosphereType() {}

    public AtmosphereType(String composition, String pressure, String gear){
        this.composition = composition;
        this.pressure = pressure;
        this.survivalGearRequired = gear;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String composition;
    private String pressure;
    private String survivalGearRequired;
}
