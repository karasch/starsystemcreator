package net.marjean.starsystemcreator.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
public class Faction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "planet_id", nullable = false)
    @JsonIgnore // if you don't JsonIgnore this, you get a planet with a faction with a planet with a faction ...
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Planet planet;

    @OneToOne
    @JoinColumn(name = "faction_id")
    private FactionType factionType;

    @OneToOne
    @JoinColumn(name = "government_id")
    private GovernmentType governmentType;

}
