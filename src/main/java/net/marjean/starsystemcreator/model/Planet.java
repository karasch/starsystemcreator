package net.marjean.starsystemcreator.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Planet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    // Planet Size
    @OneToOne
    @JoinColumn(name="size_id", referencedColumnName = "id")
    private SizeType size;

    // Atmosphere Type
    @OneToOne
    @JoinColumn(name="atmosphere_id", referencedColumnName = "id")
    private AtmosphereType atmosphere;

    // Temperature
    @OneToOne
    @JoinColumn(name="temperature_id", referencedColumnName = "id")
    private TemperatureType temperature;

    // Hydrographics
    @OneToOne
    @JoinColumn(name="hydrographics_id", referencedColumnName = "id")
    private HydrographicsType hydrographicsType;

    // Population
    @OneToOne
    @JoinColumn(name="population_id", referencedColumnName = "id")
    private PopulationType population;

    // Government
    @OneToOne
    @JoinColumn(name="government_id", referencedColumnName = "id")
    private GovernmentType governmentType;

    // "factions" might more accurately be called "rival factions".  Groups with some influence, but not the dominant government on the planet.
    //TODO factions should probably get names.
    @OneToMany(mappedBy = "planet", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Faction> factions;

    //// Cultural Differences

    // Law Level
    @OneToOne
    @JoinColumn(name="law_level_id", referencedColumnName = "id")
    private LawLevelType lawLevel;
    //// Law & Travellers

    // Star Port
    @OneToOne(mappedBy = "planet", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    StarPort starPort;
    //// Berthing cost
    //// Bases

    // Tech Level
    @OneToOne
    @JoinColumn(name="tech_level_id", referencedColumnName = "id")
    private TechLevelType techLevel;

    // Travel Zone
    @OneToOne
    @JoinColumn(name="travel_zone_id", referencedColumnName = "id")
    private TravelZoneType travelZone;

    // Trade Code
    @ManyToMany(fetch = FetchType.EAGER)
    // FetchType must be eager, or we have to implement a transaction manager.
    // https://stackoverflow.com/questions/22821695/how-to-fix-hibernate-lazyinitializationexception-failed-to-lazily-initialize-a
    @JoinTable(
            name = "planet_trade_code",
            joinColumns = @JoinColumn(name = "planet_id"),
            inverseJoinColumns = @JoinColumn(name = "trade_code_id")
    )
    private Set<TradeCodeType> tradeCodes;


}
