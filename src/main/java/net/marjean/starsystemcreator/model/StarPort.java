package net.marjean.starsystemcreator.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
public class StarPort {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "planet_id", nullable = false)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Planet planet;

    @OneToOne
    @JoinColumn(name = "star_port_id", referencedColumnName = "id")
    private StarPortType starPortType;

    private Integer berthingCost;
    private Boolean navalBase;
    private Boolean scoutBase;
    private Boolean researchBase;
    private Boolean travellersAidSociety;
}
