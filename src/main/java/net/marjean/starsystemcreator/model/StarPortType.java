package net.marjean.starsystemcreator.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class StarPortType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String starPortClass;
    private String quality;
    private String berthingCost;
    private String fuel;
    private String facilities;
    private String bases;

    @JsonIgnore
    private Integer minRoll;
    @JsonIgnore
    private Integer maxRoll;
    @JsonIgnore
    private Integer navalBaseRoll;
    @JsonIgnore
    private Integer scoutBaseRoll;
    @JsonIgnore
    private Integer researchBaseRoll;
    @JsonIgnore
    private Integer tasRoll;
    @JsonIgnore
    private Integer berthCostMultiplier;
}
