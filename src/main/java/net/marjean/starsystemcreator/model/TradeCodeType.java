package net.marjean.starsystemcreator.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class TradeCodeType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String classification;

    // Trade codes have certain requirements, that cannot be implemented in a two dimensional table (at least not without doing string processing)

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "trade_code_planet_size",
            joinColumns = @JoinColumn(name = "trade_code_id"),
            inverseJoinColumns = @JoinColumn(name = "planet_size_id")
    )
    private Set<SizeType> planetSize;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "trade_code_atmosphere",
            joinColumns = @JoinColumn(name = "trade_code_id"),
            inverseJoinColumns = @JoinColumn(name = "atmosphere_id")
    )
    private Set<AtmosphereType> planetAtmosphere;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "trade_code_hydrographics",
            joinColumns = @JoinColumn(name = "trade_code_id"),
            inverseJoinColumns = @JoinColumn(name = "hydrographics_id")
    )
    private Set<HydrographicsType> planetHydrographics;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "trade_code_population",
            joinColumns = @JoinColumn(name = "trade_code_id"),
            inverseJoinColumns = @JoinColumn(name = "population_id")
    )
    private Set<PopulationType> planetPopulation;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "trade_code_government",
            joinColumns = @JoinColumn(name = "trade_code_id"),
            inverseJoinColumns = @JoinColumn(name = "government_id")
    )
    private Set<GovernmentType> planetGovernment;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "trade_code_law_level",
            joinColumns = @JoinColumn(name = "trade_code_id"),
            inverseJoinColumns = @JoinColumn(name = "law_level_id")
    )
    private Set<LawLevelType> planetLawLevel;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "trade_code_tech_level",
            joinColumns = @JoinColumn(name = "trade_code_id"),
            inverseJoinColumns = @JoinColumn(name = "tech_level_id")
    )
    private Set<TechLevelType> planetTechLevel;
}
