package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.AtmosphereType;
import org.springframework.data.repository.CrudRepository;

public interface AtmosphereTypeRepository extends CrudRepository<AtmosphereType, Integer> {


}
