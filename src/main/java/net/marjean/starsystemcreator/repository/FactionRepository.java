package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.Faction;
import org.springframework.data.repository.CrudRepository;

public interface FactionRepository extends CrudRepository<Faction, Integer> {
}
