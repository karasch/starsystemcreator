package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.FactionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FactionTypeRepository extends JpaRepository<FactionType, Integer> {

    @Query(
            value = "SELECT * FROM faction_type WHERE min_roll <= ?1 AND max_roll >= ?1",
            nativeQuery = true
    )
    public FactionType findFactionByRoll(Integer roll);
}
