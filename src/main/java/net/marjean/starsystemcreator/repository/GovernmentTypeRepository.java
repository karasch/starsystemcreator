package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.GovernmentType;
import org.springframework.data.repository.CrudRepository;

public interface GovernmentTypeRepository extends CrudRepository<GovernmentType, Integer> {
}
