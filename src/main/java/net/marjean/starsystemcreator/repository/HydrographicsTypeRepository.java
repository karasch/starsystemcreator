package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.HydrographicsType;
import org.springframework.data.repository.CrudRepository;

public interface HydrographicsTypeRepository extends CrudRepository<HydrographicsType, Integer> {
}
