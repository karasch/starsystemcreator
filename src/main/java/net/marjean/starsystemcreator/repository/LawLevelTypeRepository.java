package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.LawLevelType;
import org.springframework.data.repository.CrudRepository;

public interface LawLevelTypeRepository extends CrudRepository<LawLevelType, Integer> {
}
