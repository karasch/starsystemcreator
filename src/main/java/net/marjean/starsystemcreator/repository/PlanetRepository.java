package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.Planet;
import org.springframework.data.repository.CrudRepository;

public interface PlanetRepository extends CrudRepository<Planet, Integer> {
}
