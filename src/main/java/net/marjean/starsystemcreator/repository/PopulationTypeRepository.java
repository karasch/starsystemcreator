package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.PopulationType;
import org.springframework.data.repository.CrudRepository;

public interface PopulationTypeRepository extends CrudRepository<PopulationType, Integer> {
}
