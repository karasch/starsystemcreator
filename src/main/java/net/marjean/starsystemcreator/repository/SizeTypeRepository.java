package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.SizeType;
import org.springframework.data.repository.CrudRepository;

public interface SizeTypeRepository extends CrudRepository<SizeType, Integer> {
}
