package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.StarPortType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StarPortTypeRepository extends CrudRepository<StarPortType, Integer> {

    @Query(
            value = "SELECT * FROM star_port_type WHERE min_roll <= ?1 AND max_roll >= ?1",
            nativeQuery = true
    )
    public Optional<StarPortType> findStarPortByRoll(Integer roll);
}
