package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.TechLevelType;
import org.springframework.data.repository.CrudRepository;

public interface TechLevelTypeRepository extends CrudRepository<TechLevelType, Integer> {
}
