package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.TemperatureType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TemperatureTypeRepository extends CrudRepository<TemperatureType, Integer> {

    @Query(
            value = "SELECT * FROM temperature_type WHERE min_roll <= ?1 AND max_roll >= ?1",
            nativeQuery = true
    )
    public Optional<TemperatureType> findTemperatureByRoll(Integer roll);
}
