package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.TradeCodeType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TradeCodeTypeRepository extends CrudRepository<TradeCodeType, Integer> {

}
