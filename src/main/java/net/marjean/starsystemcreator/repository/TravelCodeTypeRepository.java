package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.TravelZoneType;
import org.springframework.data.repository.CrudRepository;

public interface TravelCodeTypeRepository extends CrudRepository<TravelZoneType, Integer> {
}
