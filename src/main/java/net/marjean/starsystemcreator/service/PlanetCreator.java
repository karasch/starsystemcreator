package net.marjean.starsystemcreator.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.marjean.starsystemcreator.model.*;
import net.marjean.starsystemcreator.repository.*;
import net.marjean.starsystemcreator.util.DieRoller;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Service
@Slf4j
public class PlanetCreator {

    private final DieRoller dice;
    private final SizeTypeRepository sizeTypeRepository;
    private final AtmosphereTypeRepository atmosphereTypeRepository;
    private final TemperatureTypeRepository temperatureTypeRepository;
    private final HydrographicsTypeRepository hydrographicsTypeRepository;
    private final PopulationTypeRepository populationTypeRepository;
    private final GovernmentTypeRepository governmentTypeRepository;
    private final FactionTypeRepository factionTypeRepository;
    private final LawLevelTypeRepository lawLevelTypeRepository;
    private final StarPortTypeRepository starPortTypeRepository;
    private final TechLevelTypeRepository techLevelTypeRepository;
    private final TravelCodeTypeRepository travelZoneTypeRepository;
    private final TradeCodeTypeRepository tradeCodeTypeRepository;

    public Planet createRandomPlanet(){

        Planet planet = new Planet();

        int planetSize = getPlanetSize();
        SizeType size = sizeTypeRepository.findById(planetSize).get();
        planet.setSize(size);

        int atmosphere = getAtmosphere(planetSize);
        AtmosphereType atmo = atmosphereTypeRepository.findById(atmosphere).get();
        planet.setAtmosphere(atmo);

        int temperature = getTemperature(atmosphere);
        TemperatureType temp = temperatureTypeRepository.findTemperatureByRoll(temperature).get();
        planet.setTemperature(temp);

        int hydrographic = getHydrographic(planetSize, atmosphere, temperature);
        HydrographicsType hydrographicsType = hydrographicsTypeRepository.findById(hydrographic).get();
        planet.setHydrographicsType(hydrographicsType);

        int population = getPopulation();
        PopulationType pt = populationTypeRepository.findById(population).get();
        planet.setPopulation(pt);

        int government = getGovernment(population);
        GovernmentType govType = governmentTypeRepository.findById(government).get();
        planet.setGovernmentType(govType);

        // roll for # of rival factions: 1d3 + government modifier
        /// government 0,7 : +1
        /// government 10+ : -1
        /// default: 0
        int govFactionsMod = 0;
        if(government == 0 || government == 7){
            govFactionsMod = 1;
        }
        else if(government <= 10){
            govFactionsMod = -1;
        }



        if (population > 0) { // Not in the rules, but it's tough to have factions with no people.
            int numberOfFactions = dice.rollD3() + govFactionsMod;
            log.debug("# of factions: {}", numberOfFactions);

            Set<Faction> rivalFactions = new HashSet<>();
            /// for each rival faction:
            for(int i=0; i<numberOfFactions; ++i) {
                //// roll for faction/government type (see above)
                int factionGovernment = getGovernment(population);
                GovernmentType g = governmentTypeRepository.findById(factionGovernment).get();

                //// roll for faction strength: 2d6
                int factionRoll = dice.roll2d6();
                log.debug("Faction strength: {}", factionRoll);
                FactionType factionType = factionTypeRepository.findFactionByRoll(factionRoll);

                Faction f = new Faction();
                f.setGovernmentType(g);
                f.setFactionType(factionType);
                f.setPlanet(planet);

                rivalFactions.add(f);
            }

            planet.setFactions(rivalFactions);
        }


        //TODO cultural differences

        int lawLevel = getLawLevel(government);
        LawLevelType llt = lawLevelTypeRepository.findById(lawLevel).get();
        planet.setLawLevel(llt);

        int starportScore = getStarportScore(population);

        StarPort sp = getStarPort(planet, starportScore);

        planet.setStarPort(sp);


        int techLevel = getTechLevel(planetSize, atmosphere, hydrographic, population, government, sp);

        TechLevelType tlt = techLevelTypeRepository.findById(techLevel).get();
        planet.setTechLevel(tlt);

        // determine travel Zone:
        // Yellow if all of the following:
        /// Atmosphere 10+
        /// Government 0,7, or 10
        /// Law level 0 or 9+
        if (atmosphere >= 10 && (government == 0 || government == 7 || government == 10) && (lawLevel == 0 || lawLevel >= 9)){
            TravelZoneType zoneType = travelZoneTypeRepository.findById(1).get();
            planet.setTravelZone(zoneType);
        }
        // Red if plot appropriate, else N/A
        else {
            TravelZoneType zoneType = travelZoneTypeRepository.findById(3).get();
            planet.setTravelZone(zoneType);
        }


        Set<TradeCodeType> tradeCodes = getTradeCodeTypes(planet);

        planet.setTradeCodes(tradeCodes);

        return planet;
    }

    int getTechLevel(int planetSize, int atmosphere, int hydrographic, int population, int government, StarPort sp) {
        int techLevelModifier = 0;
        // roll for tech level: 1d6 + modifiers
        /// star port:
        //switch (starportType.getId()) {
        switch (sp.getStarPortType().getId()){
            //// A(1): 6
            case 1:
                techLevelModifier += 6;
                break;
            //// B(2): 4
            case 2:
                techLevelModifier += 4;
                break;
            //// C(3): 2
            case 3:
                techLevelModifier += 2;
                break;
            //// X(6): -4
            case 6:
                techLevelModifier -= 4;
        }
        /// size:
        switch (planetSize) {
            //// 0,1: 2
            case 0:
            case 1:
                techLevelModifier += 2;
                break;
            //// 2-4: 1
            case 2:
            case 3:
            case 4:
                techLevelModifier += 1;
        }
        /// atmo:
        //// 0-3,10-15: 1
        if(atmosphere <= 3 || atmosphere >= 10){
            techLevelModifier += 1;
        }
        /// hydro:
        switch (hydrographic) {
            //// 0,9: 1
            case 0:
            case 9:
                techLevelModifier += 1;
                break;
            //// 10: 2
            case 10:
                techLevelModifier += 2;
        }
        /// population:
        switch (population) {
            //// 1-5,8: 1
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
                techLevelModifier += 1;
                break;
            //// 9: 2
            case 9:
                techLevelModifier += 2;
                break;
            //// 10: 4
            case 10:
                techLevelModifier += 4;
        }
        /// government:
        switch (government) {
            //// 0,5: 1
            case 0:
            case 5:
                techLevelModifier += 1;
                break;
            //// 7: 2
            case 2:
                techLevelModifier += 2;
                break;
            //// 13,14: -2
            case 13:
            case 14:
                techLevelModifier -= 2;
        }

        log.debug("Tech Level Modifier = {}", techLevelModifier);
        int techLevel = dice.rollD6() + techLevelModifier;
        if(techLevel < 0){
            techLevel = 0;
        }
        else if(techLevel > 15){
            techLevel = 15;
        }
        log.debug("Tech Level: {}", techLevel);
        return techLevel;
    }

    //@org.jetbrains.annotations.NotNull
    StarPort getStarPort(Planet planet, int starportScore) {
        StarPort sp = new StarPort();
        sp.setPlanet(planet);

        StarPortType starportType = starPortTypeRepository.findStarPortByRoll(starportScore).get();
        sp.setStarPortType(starportType);

        // roll star port berthing cost
        int berthCostRoll = dice.rollD6();
        sp.setBerthingCost(berthCostRoll * starportType.getBerthCostMultiplier());
        // roll for base types
        int navalBaseRoll = dice.roll2d6();
        log.debug("Roll for Naval Base: {}", navalBaseRoll);
        if(navalBaseRoll >= starportType.getNavalBaseRoll()){
            sp.setNavalBase(true);
        }

        int researchBaseRoll = dice.roll2d6();
        log.debug("Roll for Research Base: {}", researchBaseRoll);
        if(researchBaseRoll >= starportType.getResearchBaseRoll()){
            sp.setResearchBase(true);
        }

        int scoutBaseRoll = dice.roll2d6();
        log.debug("Roll for Scout Base: {}", scoutBaseRoll);
        if(scoutBaseRoll >= starportType.getScoutBaseRoll()){
            sp.setScoutBase(true);
        }
        int tasRoll = dice.roll2d6();
        log.debug("Roll for TAS: {}", tasRoll);
        if(tasRoll >= starportType.getTasRoll()){
            sp.setTravellersAidSociety(true);
        }
        return sp;
    }

    //@org.jetbrains.annotations.NotNull
    //TODO find out if there's a non-jetbrains-specific version of NotNull
    public Set<TradeCodeType> getTradeCodeTypes(Planet planet) {
        // Trade Codes...
        Set<TradeCodeType> tradeCodes = new HashSet<>();
        // For each type of trade code
        for (TradeCodeType tc:tradeCodeTypeRepository.findAll() ) {
            //If there is at least one relationship between this trade code and planet size
            if(tc.getPlanetSize().isEmpty()) {
                // No requirement for this trade code and this planet attribute
                log.debug("No size requirement for {}", tc.getClassification());
            }
            else{
                // Check if this planet's size meets the requirement.
                if(tc.getPlanetSize().contains(planet.getSize())){
                    // so far, so good
                    log.debug("Planet size requirement met for {}", tc.getClassification());
                }
                else{
                    // it doesn't line up, go to the next TradeCode
                    log.debug("Planet size requirement failed for {}", tc.getClassification());
                    continue;
                }
            }

            //atmosphere
            if(tc.getPlanetAtmosphere().isEmpty()) {
                log.debug("No atmosphere requirement for {}", tc.getClassification());
            }
            else{
                if(tc.getPlanetAtmosphere().contains(planet.getAtmosphere())){
                    log.debug("Atmosphere requirement met for {}", tc.getClassification());
                }
                else{
                    log.debug("Atmosphere requirement failed for {}", tc.getClassification());
                    continue;
                }
            }
            //hydrographics
            if(tc.getPlanetHydrographics().isEmpty()) {
                log.debug("No hydrographic requirement for {}", tc.getClassification());
            }
            else{
                if(tc.getPlanetHydrographics().contains(planet.getHydrographicsType())){
                    log.debug("Hydrographics requirement met for {}", tc.getClassification());
                }
                else{
                    log.debug("Hydrographics requirement failed for {}", tc.getClassification());
                    continue;
                }
            }
            //population
            if(tc.getPlanetPopulation().isEmpty()) {
                log.debug("No population requirement for {}", tc.getClassification());
            }
            else{
                if(tc.getPlanetPopulation().contains(planet.getPopulation())){
                    log.debug("Population requirement met for {}", tc.getClassification());
                }
                else{
                    log.debug("Population requirement failed for {}", tc.getClassification());
                    continue;
                }
            }
            //government
            if(tc.getPlanetGovernment().isEmpty()) {
                log.debug("No government requirement for {}", tc.getClassification());
            }
            else{
                if(tc.getPlanetGovernment().contains(planet.getGovernmentType())){
                    log.debug("Government requirement met for {}", tc.getClassification());
                }
                else{
                    log.debug("Planet size requirement failed for {}", tc.getClassification());
                    continue;
                }
            }
            //law level
            if(tc.getPlanetLawLevel().isEmpty()) {
                log.debug("No law level requirement for {}", tc.getClassification());
            }
            else{
                if(tc.getPlanetLawLevel().contains(planet.getLawLevel())){
                    log.debug("Law level requirement met for {}", tc.getClassification());
                }
                else{
                    log.debug("Law level requirement failed for {}", tc.getClassification());
                    continue;
                }
            }
            //tech level
            if(tc.getPlanetTechLevel().isEmpty()) {
                log.debug("No tech level requirement for {}", tc.getClassification());
            }
            else{
                if(tc.getPlanetTechLevel().contains(planet.getTechLevel())){
                    log.debug("Tech level requirement met for {}", tc.getClassification());
                }
                else{
                    log.debug("Tech level requirement failed for {}", tc.getClassification());
                    continue;
                }
            }

            // we have passed all the validations, assign the travel code!
            tradeCodes.add(tc);


        }
        return tradeCodes;
    }

    int getStarportScore(int population) {
        // roll for star port class: 2d6 + population modifier
        // min 2, max 11
        int starportPopMod;
        /// population: modifier
        switch (population) {
            /// 0-2: -2
            case 0:
            case 1:
            case 2:
                starportPopMod = -2;
                break;
            /// 3-4: -1
            case 3:
            case 4:
                starportPopMod = -1;
                break;
            /// 5-7: 0
            case 5:
            case 6:
            case 7:
                starportPopMod = 0;
                break;
            /// 8-9: +1
            case 8:
            case 9:
                starportPopMod = 1;
                break;
            /// 10+: +2
            case 10:
            default:
                starportPopMod = 2;
        }
        int starportScore = dice.roll2d6() + starportPopMod;
        if(starportScore < 2){
            starportScore = 2;
        }
        else if (starportScore > 11){
            starportScore = 11;
        }
        return starportScore;
    }

    int getLawLevel(int government) {
        // roll for law level: 2d6 - 7 + government stat
        // min 0, max 9
        int lawLevel = dice.roll2d6() - 7 + government;
        log.debug("Pre-mod law level: {}", lawLevel);
        if(lawLevel < 0){
            lawLevel = 0;
        }
        else if(lawLevel > 9){
            lawLevel = 9;
        }
        log.debug("Law Level = {}", lawLevel);
        return lawLevel;
    }

    int getGovernment(int population) {
        // roll for government
        /// government = 0 if population == 0
        /// else roll 2d6 - 7 + population stat
        /// min 0, max 15
        int government;
        if(population == 0){
            government = 0;
        }
        else{
            government = dice.roll2d6() - 7 + population;
            if(government < 0){
                government = 0;
            }
            else if(government > 15){
                government = 15;
            }
        }
        log.debug("Government = {}", government);
        return government;
    }

    int getPopulation() {
        // roll for population: 2d6 - 2
        int population = dice.roll2d6() - 2;
        log.debug("Population = {}", population);
        return population;
    }

    int getHydrographic(int planetSize, int atmosphere, int temperature) {
        // roll for hydrographics:
        int hydrographic;
        /// if planet size == 0 or planet size == 1 then hydro = 0
        if(planetSize == 0 || planetSize == 1){
            hydrographic = 0;
        }
        /// else, roll 2d6 + atmosphere modifier
        /// min 0, max 10
        else{
            int atmoHydroModifier;
            /// Atmosphere modifiers:
            /// 0,1,10,11,12: -4
            /// default: + atmosphere stat
            switch (atmosphere){
                case 0:
                case 1:
                case 10:
                case 11:
                case 12:
                    atmoHydroModifier = -4;
                default:
                    atmoHydroModifier = atmosphere;
            }

            /// if Atmosphere is not 13
            if(atmosphere != 13) {
                /// ... and temperature is hot (10-11): hydrographics -= 2
                if(temperature == 10 || temperature == 11){
                    atmoHydroModifier -= 2;
                }
                /// ... and temperature is boiling (12): hydrographics -= 6
                if(temperature == 12){
                    atmoHydroModifier -= 6;
                }
            }

            hydrographic = dice.roll2d6() + atmoHydroModifier;
            if(hydrographic < 0){
                hydrographic = 0;
            }
            else if(hydrographic > 10){
                hydrographic = 10;
            }
        }

        log.debug("Hydro type: {}", hydrographic);
        return hydrographic;
    }

    int getTemperature(int atmosphere) {
        // roll for temperature: 2d6 + atmosphere modifier
        int atmoModifier;
        switch (atmosphere) {
            /// 0,1,6,7: 0
            case 0:
            case 1:
            case 6:
            case 7:
                atmoModifier = 0;
                break;
            /// 2,3: -2
            case 2:
            case 3:
                atmoModifier = -2;
                break;
            /// 4,5,14: -1
            case 4:
            case 5:
            case 14:
                atmoModifier = -1;
                break;
            /// 8,9: 1
            case 8:
            case 9:
                atmoModifier = 1;
                break;
            /// 10,13,15: 2
            case 10:
            case 13:
            case 15:
                atmoModifier = 2;
                break;
            /// 11,12: 6
            case 11:
            case 12:
            default:
                atmoModifier = 6;
        }
        //NOTE: Atmo 0,1 change a lot between day and night.
        //NOTE: Additional modifiers of -4 to +4 can be added for plot reasons.
        int temperature = dice.roll2d6() + atmoModifier;
        // min 2, max 12
        if(temperature < 2){
            temperature = 2;
        }
        if(temperature > 12){
            temperature = 12;
        }
        log.debug("Temperature = {}", temperature);
        return temperature;
    }

    int getAtmosphere(int planetSize) {
        // roll for atmosphere: 2d6 - 7 + planet size
        // min 0, max 15 - we won't hit 16, but we'll need to guard against negatives
        int atmosphere = dice.roll2d6() - 7 + planetSize;
        if(atmosphere < 0){
            atmosphere = 0;
        }
        log.debug("atmosphere = {}", atmosphere);
        return atmosphere;
    }

    int getPlanetSize() {
        // roll for size: 2d6-2
        int planetSize = dice.roll2d6() -2;
        log.debug("Planet size = {}", planetSize);
        return planetSize;
    }
}
