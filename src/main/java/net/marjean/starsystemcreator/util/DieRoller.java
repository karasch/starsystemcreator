package net.marjean.starsystemcreator.util;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class DieRoller {

    private Random die;

    public DieRoller(){
        die = new Random();
    }

    public DieRoller(long seed){
        die = new Random(seed);
    }

    public int rollD6(){
        return die.nextInt(5) +1;
    }

    public int roll2d6(){
        return die.nextInt(5) + die.nextInt(5) +2;
    }

    public int rollD3(){
        return die.nextInt(2) +1;
    }
}
