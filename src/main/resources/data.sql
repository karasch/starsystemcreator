/*** Dimensions ***/
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (0, 'None', '0.00', 'Vacc Suit');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (1, 'Trace', '0.001 to 0.09', 'Vacc Suit');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (2, 'Very Thin, Tainted', '0.1 to 0.42', 'Respirator, Filter');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (3, 'Very Thin', '0.1 to 0.42', 'Respirator');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (4, 'Thin, Tainted', '0.43 to 0.7', 'Filter');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (5, 'Thin', '0.43 to 0.7', null);
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (6, 'Standard', '0.71 to 1.49', null);
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (7, 'Standard, Tainted', '0.71 to 1.49', 'Filter');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (8, 'Dense', '1.5 to 2.49', null);
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (9, 'Dense, Tainted', '1.5 to 2.49', 'Filter');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (10, 'Exotic', 'Varies', 'Air Supply');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (11, 'Corrosive', 'Varies', 'Vacc Suit');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (12, 'Insidious', 'Varies', 'Vacc Suit');
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (13, 'Very Dense', '2.5+', null);
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (14, 'Low', '0.5 or less', null);
INSERT INTO atmosphere_type (id, composition, pressure, survival_gear_required) VALUES (15, 'Unusual', 'Varies', 'Varies');

INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (0, 'Less than 1000', 'Asteroid, orbital complex', 'Negligible');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (1, '1,600', 'Triton', '0.05');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (2, '3,200', 'Luna, Europa', '0.15');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (3, '4,800', 'Mercury, Ganymede', '0.25');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (4, '6,400', null, '0.35');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (5, '8,000', 'Mars', '0.45');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (6, '9,600', null, '0.7');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (7, '11,200', null, '0.9');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (8, '12,800', 'Earth', '1.0');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (9, '14,400', null, '1.25');
INSERT INTO planet_size_type (id, diameter, example, surface_gravity) VALUES (10, '16,000', null, '1.4');

INSERT INTO temperature_type (id, type, average_temperature, description, min_roll, max_roll) VALUES (1, 'Frozen', '-51 or less', 'Frozen world.  No liquid water, very dry atmosphere', 0 ,2);
INSERT INTO temperature_type (id, type, average_temperature, description, min_roll, max_roll) VALUES (2, 'Cold', '-50 to 0', 'Icy world.  Little liquid water, extensive ice caps, few clouds.', 3, 4);
INSERT INTO temperature_type (id, type, average_temperature, description, min_roll, max_roll) VALUES (3, 'Temperate', '0 to 30', 'Temperate world.  Earth-like. Liquid & vaporised water are common, moderate ice caps.', 5, 9);
INSERT INTO temperature_type (id, type, average_temperature, description, min_roll, max_roll) VALUES (4, 'Hot', '31 to 80', 'Hot world. Small or no ice caps, little liquid water. Most water in the form of clouds.', 10, 11);
INSERT INTO temperature_type (id, type, average_temperature, description, min_roll, max_roll) VALUES (5, 'Boiling', '81+', 'Boiling world. No ice caps, little liquid water.', 12, 12);

INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (0, '0-5%', 'Desert World');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (1, '6-15%', 'Dry World');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (2, '16-25%', 'A few small seas');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (3, '26-35%', 'Small seas and oceans');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (4, '36-45%', 'Wet world');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (5, '46-55%', 'Large oceans');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (6, '56-65%', null);
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (7, '66-75%', 'Earth-like world');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (8, '75-85%', 'Water world');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (9, '86-95%', 'Only a few small islands and archipelagos');
INSERT INTO hydrographics_type (id, hydrographic_percentage, description) VALUES (10, '96-100%', 'Almost entirely water');

INSERT INTO population_type (id, inhabitants, range, description) VALUES (0, 'None', '0', NULL);
INSERT INTO population_type (id, inhabitants, range, description) VALUES (1, 'Few', '1+', 'A tiny farmstead or a single family');
INSERT INTO population_type (id, inhabitants, range, description) VALUES (2, 'Hundreds', '100+', 'A village');
INSERT INTO population_type (id, inhabitants, range, description) VALUES (3, 'Thousands', '1,000+', NULL);
INSERT INTO population_type (id, inhabitants, range, description) VALUES (4, 'Tens of thousands', '10,000+', 'Small town');
INSERT INTO population_type (id, inhabitants, range, description) VALUES (5, 'Hundreds of thousands', '100,000+', 'Average city');
INSERT INTO population_type (id, inhabitants, range, description) VALUES (6, 'Millions', '1,000,000+', NULL);
INSERT INTO population_type (id, inhabitants, range, description) VALUES (7, 'Tens of millions', '10,000,000+', 'Large city');
INSERT INTO population_type (id, inhabitants, range, description) VALUES (8, 'Hundreds of millions', '100,000,000+', NULL);
INSERT INTO population_type (id, inhabitants, range, description) VALUES (9, 'Billions', '1,000,000,000+', 'Present day Earth');
INSERT INTO population_type (id, inhabitants, range, description) VALUES (10, 'Tens of billions', '10,000,000,000+', NULL);
INSERT INTO population_type (id, inhabitants, range, description) VALUES (11, 'Hundreds of billions', '100,000,000,000', 'Incredibly crowded world');
INSERT INTO population_type (id, inhabitants, range, description) VALUES (12, 'Trillions', '1,000,000,000,000+', 'World-city');

INSERT INTO base_type (id, name) VALUES (1, 'Naval Base');

/*INSERT INTO gas_giant_type (id, name) VALUES (1, 'Hydrogen');*/

INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (0, 'None', 'No government structure.  In many cases, family bonds predominate', 'Family, Clan, Anarchy', 'None');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (1, 'Company/Corporation', 'Ruling functions are assumed by a company managerial elite, and most citizenry are company employees or dependants', 'Corporate outpost, asteroid mine, feudal domain', 'Weapons, Drugs, Travellers');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (2, 'Participating democracy', 'Ruling functions are reached by the advice and consent of the citizenry directly', 'Collective, tribal council, comm-linked consensus', 'Drugs');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (3, 'Self-perpetuating oligarchy', 'Ruling functions are performed by a restricted minority, with little or no input from the mass of citizenry', 'Plutocracy, hereditary ruling caste', 'Technology, weapons, travellers');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (4, 'Representative democracy', 'Ruling functions are performed by elected representatives', 'Republic, democracy', 'Drugs, weapons, psionics');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (5, 'Feudal technocracy', 'Ruling functions are performed by specific individuals for persons who agreed to be ruled by them.  Relationshipsa are based on the performance of technical activities which are mutually beneficial.', 'Those with access to higher technology tend to have higher social status', 'Technology, weapons, computers');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (6, 'Captive government', 'Ruling functions are performed by an imposed leadership answerable to an outside group', 'A colony or conquered area', 'Weapons, technology, travellers');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (7, 'Balkanization', 'No central authority exists; rival governments compete for control.  Law level refers to the government nearest the star port.', 'Multiple governments, civil war', 'Varies');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (8, 'Civil service bureaucracy', 'Ruling functions are performed by government agencies employing individuals selected for their expertise.', 'Technocracy, communism', 'Drugs, weapons');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (9, 'Impersonal bureaucracy', 'Ruling functions are performed by agencies that have become insulated from the governed citizens.', 'Entrenched castes of bureaucrats, decaying empire', 'Technology, weapons, drugs, travellers, psionics');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (10, 'Charismatic dictator', 'Ruling functions are performed by agencies directed by a single leader who enjoys overwhelming confidence of the citizens.', 'Revolutionary leader, messiah, emperor', 'None');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (11, 'Non-charismatic leader', 'A previous charismatic dictator has been replaced by a leader through normal channels.', 'Military dictatorship, hereditary kingship', 'Weapons, technology, computers');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (12, 'Charismatic oligarchy', 'Ruling functions are performed by a select group of members of an organisation or class which enjoys overwhelming confidence of the citizenry.', 'Junta, revolutionary council', 'Weapons');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (13, 'Religious dictatorship', 'Ruling functions are performed by a religious organization without regard to the specific individual needs of the citizenry.', 'Cult, transcendent philosophy, psionic group mind', 'Varies');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (14, 'Religious autocracy', 'Government by a single religious leader having absolute power over the citizenry', 'Messiah', 'Varies');
INSERT INTO government_type (id, name, description, examples, common_contraband) VALUES (15, 'Totalitarian oligarchy', 'Government by an all-powerful minority which maintains absolute control through widespread coercion and oppression', 'World church, ruthless corporation', 'Varies');


INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (0, 'No restrictions - heavy armor and a handy blaster weapon recommended...', NULL);
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (1, 'Poison gas, explosives, undetectable weapons, WMD', 'Battle dress');
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (2, 'Portable energy and laser weapons', 'Combat armor');
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (3, 'Military weapons', 'Flak');
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (4, 'Light assault weapons and submachine guns', 'Cloth');
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (5, 'Personal concealable weapons', 'Mesh');
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (6, 'All firearms except shotguns & stunners; carrying concealed weapons discouraged', NULL);
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (7, 'Shotguns', NULL);
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (8, 'All bladed weapons, stunners', 'All visible armor');
INSERT INTO law_level_type (id, weapons_banned, armor_banned) VALUES (9, 'All weapons', 'All armor');

INSERT INTO star_port_type (id, star_port_class, quality, berthing_cost, fuel, facilities, bases, min_roll, max_roll, naval_base_roll, scout_base_roll, research_base_roll, tas_roll, berth_cost_multiplier) VALUES (1, 'A', 'Excellent', '1D x Cr1000', 'Refined', 'Shipyard (all), Repair', 'Naval 8+, Scout 10+, Research 8+, TAS', 11, 11, 8, 10, 8, 1, 1000);
INSERT INTO star_port_type (id, star_port_class, quality, berthing_cost, fuel, facilities, bases, min_roll, max_roll, naval_base_roll, scout_base_roll, research_base_roll, tas_roll, berth_cost_multiplier) VALUES (2, 'B', 'Good', '1D x Cr500', 'Refined', 'Shipyard (spacecraft), Repair', 'Naval 8+, Scout 8+, Research 10+, TAS', 9, 10, 8, 8, 10, 1, 500);
INSERT INTO star_port_type (id, star_port_class, quality, berthing_cost, fuel, facilities, bases, min_roll, max_roll, naval_base_roll, scout_base_roll, research_base_roll, tas_roll, berth_cost_multiplier) VALUES (3, 'C', 'Routine', '1D x Cr100', 'Unrefined', 'Shipyard (small craft), Repair', 'Scout 8+, Research 10+, TAS 10+', 7, 8, 13, 8, 10, 10, 100);
INSERT INTO star_port_type (id, star_port_class, quality, berthing_cost, fuel, facilities, bases, min_roll, max_roll, naval_base_roll, scout_base_roll, research_base_roll, tas_roll, berth_cost_multiplier) VALUES (4, 'D', 'Poor', '1D x Cr10', 'Unrefined', 'Limited Repair', 'Scout 7+', 5, 6, 13, 7, 13, 13, 10);
INSERT INTO star_port_type (id, star_port_class, quality, berthing_cost, fuel, facilities, bases, min_roll, max_roll, naval_base_roll, scout_base_roll, research_base_roll, tas_roll, berth_cost_multiplier) VALUES (5, 'E', 'Frontier', '0', 'None', 'None', 'None', 3, 4, 13, 13, 13, 13, 0);
INSERT INTO star_port_type (id, star_port_class, quality, berthing_cost, fuel, facilities, bases, min_roll, max_roll, naval_base_roll, scout_base_roll, research_base_roll, tas_roll, berth_cost_multiplier) VALUES (6, 'X', 'No starport', '0', 'None', 'None', 'None', 2, 2, 13, 13, 13, 13, 0);

INSERT INTO tech_level_type (id, score) VALUES (0, '0');
INSERT INTO tech_level_type (id, score) VALUES (1, '1');
INSERT INTO tech_level_type (id, score) VALUES (2, '2');
INSERT INTO tech_level_type (id, score) VALUES (3, '3');
INSERT INTO tech_level_type (id, score) VALUES (4, '4');
INSERT INTO tech_level_type (id, score) VALUES (5 ,'5');
INSERT INTO tech_level_type (id, score) VALUES (6 ,'6');
INSERT INTO tech_level_type (id, score) VALUES (7 ,'7');
INSERT INTO tech_level_type (id, score) VALUES (8, '8');
INSERT INTO tech_level_type (id, score) VALUES (9, '9');
INSERT INTO tech_level_type (id, score) VALUES (10, '10');
INSERT INTO tech_level_type (id, score) VALUES (11, '11');
INSERT INTO tech_level_type (id, score) VALUES (12, '12');
INSERT INTO tech_level_type (id, score) VALUES (13, '13');
INSERT INTO tech_level_type (id, score) VALUES (14, '14');
INSERT INTO tech_level_type (id, score) VALUES (15, '15');
INSERT INTO tech_level_type (id, score) VALUES (16, 'X');

INSERT INTO travel_zone_type (id, name) VALUES (1, 'Yellow');
INSERT INTO travel_zone_type (id, name) VALUES (2, 'Red');
INSERT INTO travel_zone_type (id, name) VALUES (3, 'N/A');

INSERT INTO faction_type (id, min_roll, max_roll, relative_strength) VALUES (1, 2, 3, 'Obscure group - few have heard of them, no popular support');
INSERT INTO faction_type (id, min_roll, max_roll, relative_strength) VALUES (2, 4, 5, 'Fringe group - few supporters');
INSERT INTO faction_type (id, min_roll, max_roll, relative_strength) VALUES (3, 6, 7, 'Minor group - some supporters');
INSERT INTO faction_type (id, min_roll, max_roll, relative_strength) VALUES (4, 8, 9, 'Notable group - significant support, well known');
INSERT INTO faction_type (id, min_roll, max_roll, relative_strength) VALUES (5, 10, 11, 'Significant - nearly as powerful as the government');
INSERT INTO faction_type (id, min_roll, max_roll, relative_strength) VALUES (6, 12, 50, 'Overwhelming popular support - more powerful than the government');

INSERT INTO trade_code_type (id, code, classification) VALUES (1, 'Ag', 'Agricultural');
INSERT INTO trade_code_type (id, code, classification) VALUES (2, 'As', 'Asteroid');
INSERT INTO trade_code_type (id, code, classification) VALUES (3, 'Ba', 'Barren');
INSERT INTO trade_code_type (id, code, classification) VALUES (4, 'De', 'Desert');
INSERT INTO trade_code_type (id, code, classification) VALUES (5, 'Fl', 'Fluid Oceans');
INSERT INTO trade_code_type (id, code, classification) VALUES (6, 'Ga', 'Garden');
INSERT INTO trade_code_type (id, code, classification) VALUES (7, 'Hi', 'High Population');
INSERT INTO trade_code_type (id, code, classification) VALUES (8, 'Ht', 'High Tech');
INSERT INTO trade_code_type (id, code, classification) VALUES (9, 'Ic', 'Ice Capped');
INSERT INTO trade_code_type (id, code, classification) VALUES (10, 'In', 'Industrial');
INSERT INTO trade_code_type (id, code, classification) VALUES (11, 'Lo', 'Low Population');
INSERT INTO trade_code_type (id, code, classification) VALUES (12, 'Lt', 'Low Tech');
INSERT INTO trade_code_type (id, code, classification) VALUES (13, 'Na', 'Non-Agricultural');
INSERT INTO trade_code_type (id, code, classification) VALUES (14, 'NI', 'Non-Industrial');
INSERT INTO trade_code_type (id, code, classification) VALUES (15, 'Po', 'Poor');
INSERT INTO trade_code_type (id, code, classification) VALUES (16, 'Ri', 'Rich');
INSERT INTO trade_code_type (id, code, classification) VALUES (17, 'Va', 'Vacuum');
INSERT INTO trade_code_type (id, code, classification) VALUES (18, 'Wa', 'Water World');

/**** Trade code look ups ****/

/* Asteroids are Size 0 */
INSERT INTO trade_code_planet_size (trade_code_id, planet_size_id) VALUES (2,0);
/* Garden planets are size 6-8 */
INSERT INTO trade_code_planet_size (trade_code_id, planet_size_id) VALUES (6,6);
INSERT INTO trade_code_planet_size (trade_code_id, planet_size_id) VALUES (6,7);
INSERT INTO trade_code_planet_size (trade_code_id, planet_size_id) VALUES (6,8);

/* Agricultural planets have atmo 4-9 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (1,4);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (1,5);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (1,6);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (1,7);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (1,8);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (1,9);
/* Asteroids have atmo 0 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (2,0);
/* Desert planets have atmo 2-15 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,2);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,3);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,4);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,5);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,6);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,7);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,8);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,9);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,10);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,11);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,12);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,13);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,14);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (4,15);
/* Fluid oceans require atmo 10-15 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (5,10);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (5,11);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (5,12);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (5,13);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (5,14);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (5,15);
/* Garden = atmo 5,6,8 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (7,5);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (7,6);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (7,8);
/* Ice capped = atmo 0,1 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (9,0);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (9,1);
/* Industrial = atmo 0,1,2,4,7,9 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (10,0);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (10,1);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (10,2);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (10,4);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (10,7);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (10,9);
/* Non-Ag = atmo 0-3 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (13,0);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (13,1);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (13,2);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (13,3);
/* Poor = atmo 2-5 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (15,2);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (15,3);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (15,4);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (15,5);
/* Rich = atmo 6-8 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (16,6);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (16,7);
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (16,8);
/* Vacuum = atmo 0 */
INSERT INTO trade_code_atmosphere (trade_code_id, atmosphere_id) VALUES (17,0);

/* Ag worlds have hydro 4-8 */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (1,4);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (1,5);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (1,6);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (1,7);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (1,8);
/* Asteroids have no water */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (2,0);
/* Deserts also have no water */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (4,0);
/* Fluid oceans require water */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,1);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,2);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,3);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,4);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,5);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,6);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,7);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,8);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,9);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,10);
/* Garden planet = hydro 5-7 */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (6,5);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (6,6);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (6,7);
/* Ice-capped planets require water */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,1);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,2);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,3);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,4);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,5);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,6);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,7);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,8);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,9);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (5,10);
/* Non-ag = hydro 0-3 */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (13,0);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (13,1);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (13,2);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (13,3);
/* Poor = hydro 0-3 */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (15,0);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (15,1);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (15,2);
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (15,3);
/* Water World = hydro 10+ */
INSERT INTO trade_code_hydrographics (trade_code_id, hydrographics_id) VALUES (18,10);

/* Ag world = pop 5-7 */
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (1,5);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (1,6);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (1,7);
/* Barren = Pop 0 */
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (3,0);
/* High pop = pop 9+ */
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (7,9);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (7,10);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (7,11);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (7,12);
/* Industrial = pop 9+ */
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (10,9);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (10,10);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (10,11);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (10,12);
/* Low pop = 0-3 */
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (11,0);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (11,1);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (11,2);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (11,3);
/* Non-Ag = pop 6+ */
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (13,6);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (13,7);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (13,8);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (13,9);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (13,10);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (13,11);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (13,6);
/* Non-Industrial = pop 0-6 */
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (14,0);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (14,1);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (14,2);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (14,3);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (14,4);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (14,4);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (14,6);
/* Rich = pop 6-8 */
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (16,6);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (16,7);
INSERT INTO trade_code_population (trade_code_id, population_id) VALUES (16,8);

/* Barren world = no government */
INSERT INTO trade_code_government (trade_code_id, government_id) VALUES (3,0);
/* Rich world = government 4-9 */
INSERT INTO trade_code_government (trade_code_id, government_id) VALUES (16,4);
INSERT INTO trade_code_government (trade_code_id, government_id) VALUES (16,5);
INSERT INTO trade_code_government (trade_code_id, government_id) VALUES (16,6);
INSERT INTO trade_code_government (trade_code_id, government_id) VALUES (16,7);
INSERT INTO trade_code_government (trade_code_id, government_id) VALUES (16,8);
INSERT INTO trade_code_government (trade_code_id, government_id) VALUES (16,9);

/* Barren world = lawless world */
INSERT INTO trade_code_law_level (trade_code_id, law_level_id) VALUES (3,0);

/* Low tech = tech 1-6 */
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (12, 1);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (12, 2);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (12, 3);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (12, 4);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (12, 5);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (12, 6);

/* High tech = tech 13+ */
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (8, 12);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (8, 13);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (8, 14);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (8, 15);
INSERT INTO trade_code_tech_level (trade_code_id, tech_level_id) VALUES (8, 16);


/***** Testing data *****/
INSERT INTO planet (name, atmosphere_id, size_id, temperature_id, hydrographics_id, population_id, government_id, law_level_id, star_port_id, tech_level_id, travel_zone_id) VALUES ('Steve', 1, 0, 2, 0, 0, 1, 0, 1, 16, 2);
INSERT INTO planet (name, atmosphere_id, government_id) VALUES ('Bobby', 2, 1);
/*
INSERT INTO planet_faction (planet_id, faction_id) VALUES (1, 1);
INSERT INTO planet_faction (planet_id, faction_id) VALUES (1, 2);
*/
INSERT INTO faction (planet_id, government_id, faction_id) VALUES (1,1,1);
INSERT INTO faction (planet_id, government_id, faction_id) VALUES (1,2,2);

INSERT INTO star_port (planet_id, star_port_id, berthing_cost, naval_base, scout_base, research_base, travellers_aid_society) VALUES (1, 1, 1000, TRUE, FALSE, NULL, NULL);

/* NOTE: These are nonsense, but useful for testing. */
INSERT INTO planet_trade_code (planet_id, trade_code_id) VALUES (1,1);
INSERT INTO planet_trade_code (planet_id, trade_code_id) VALUES (1,2);
INSERT INTO planet_trade_code (planet_id, trade_code_id) VALUES (1,3);
INSERT INTO planet_trade_code (planet_id, trade_code_id) VALUES (1,12);