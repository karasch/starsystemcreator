
CREATE TABLE atmosphere_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    composition VARCHAR(256),
    pressure VARCHAR(256),
    survival_gear_required VARCHAR(256)
);

CREATE TABLE planet_size_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    diameter VARCHAR(256) NULL,
    example VARCHAR(256) NULL,
    surface_gravity VARCHAR(256) NULL
);

CREATE TABLE hydrographics_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    hydrographic_percentage VARCHAR(256) NULL,
    description VARCHAR(256) NULL
);

CREATE TABLE temperature_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    type VARCHAR(256) NULL,
    average_temperature VARCHAR(256) NULL,
    description VARCHAR(256) NULL,
    min_roll INTEGER,
    max_roll INTEGER
);

CREATE TABLE population_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    inhabitants VARCHAR(256) NULL,
    range VARCHAR(256) NULL,
    description VARCHAR(256) NULL
);



CREATE TABLE government_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(256) NULL,
    description VARCHAR(256) NULL,
    examples VARCHAR(256) NULL,
    common_contraband VARCHAR(256) NULL
);

CREATE TABLE law_level_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    weapons_banned VARCHAR(256) NULL,
    armor_banned VARCHAR(256) NULL
);

CREATE TABLE star_port_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    star_port_class VARCHAR(8) NULL,
    quality VARCHAR(256) NULL,
    berthing_cost VARCHAR(256) NULL,
    fuel VARCHAR(256) NULL,
    facilities VARCHAR(256) NULL,
    bases VARCHAR(256) NULL,
    min_roll INTEGER NOT NULL,
    max_roll INTEGER NOT NULL,
    naval_base_roll INTEGER NOT NULL,
    scout_base_roll INTEGER NOT NULL,
    research_base_roll INTEGER NOT NULL,
    tas_roll INTEGER NOT NULL,
    berth_cost_multiplier INTEGER NOT NULL
);

CREATE TABLE tech_level_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    score VARCHAR(8) NULL
);


CREATE TABLE travel_zone_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(256) NULL
);

CREATE TABLE faction_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    relative_strength VARCHAR(256) NULL,
    min_roll INTEGER,
    max_roll INTEGER
);

CREATE TABLE trade_code_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    classification VARCHAR(256) NULL,
    code VARCHAR(8) NULL
    /* TODO planet_size, atmosphere, hydro, population, government, law_level, tech_level */
);


CREATE TABLE planet (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(256) NULL,
    atmosphere_id INTEGER NULL,
    size_id INTEGER NULL,
    temperature_id INTEGER NULL,
    hydrographics_id INTEGER NULL,
    population_id INTEGER NULL,
    /*base_id INTEGER NULL,
    polity_id INTEGER NULL,*/
    government_id INTEGER NULL,
    law_level_id INTEGER NULL,
    star_port_id INTEGER NULL,
    tech_level_id INTEGER NULL,
    travel_zone_id INTEGER NULL,
    FOREIGN KEY (atmosphere_id) references atmosphere_type(id),
    FOREIGN KEY (size_id) references planet_size_type(id),
    FOREIGN KEY (temperature_id) references temperature_type(id),
    FOREIGN KEY (hydrographics_id) references hydrographics_type(id),
    FOREIGN KEY (population_id) references population_type(id),
    /*FOREIGN KEY (base_id) references base_type(id),
    FOREIGN KEY (polity_id) references polity_type(id),*/
    FOREIGN KEY (government_id) references government_type(id),
    FOREIGN KEY (law_level_id) references law_level_type(id),
    FOREIGN KEY (star_port_id) references star_port_type(id),
    FOREIGN KEY (tech_level_id) references tech_level_type(id),
    FOREIGN KEY (travel_zone_id) references travel_zone_type(id)
);

CREATE TABLE faction (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    planet_id INTEGER NOT NULL,
    government_id INTEGER NOT NULL,
    faction_id INTEGER NOT NULL,
    FOREIGN KEY (planet_id) references planet(id),
    FOREIGN KEY (government_id) references government_type(id),
    FOREIGN KEY (faction_id) references faction_type(id)
);

CREATE TABLE star_port (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    planet_id INTEGER NOT NULL,
    star_port_id INTEGER NOT NULL,
    berthing_cost INTEGER NULL,
    naval_base BOOLEAN NULL,
    scout_base BOOLEAN NULL,
    research_base BOOLEAN NULL,
    travellers_aid_society BOOLEAN NULL,
    FOREIGN KEY (planet_id) references planet(id),
    FOREIGN KEY (star_port_id) references star_port_type(id)
);

CREATE TABLE planet_trade_code (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    planet_id INTEGER NOT NULL,
    trade_code_id INTEGER NOT NULL,
    FOREIGN KEY (planet_id) references planet(id),
    FOREIGN KEY (trade_code_id) references trade_code_type(id)
);

CREATE TABLE base_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(256)
);

CREATE TABLE polity_type (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(256)
);

CREATE TABLE trade_code_planet_size (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    trade_code_id INTEGER NOT NULL,
    planet_size_id INTEGER NOT NULL,
    FOREIGN KEY (trade_code_id) REFERENCES trade_code_type(id),
    FOREIGN KEY (planet_size_id) REFERENCES planet_size_type(id)
);

CREATE TABLE trade_code_atmosphere (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    trade_code_id INTEGER NOT NULL,
    atmosphere_id INTEGER NOT NULL,
    FOREIGN KEY (trade_code_id) REFERENCES trade_code_type(id),
    FOREIGN KEY (atmosphere_id) REFERENCES atmosphere_type(id)
);

CREATE TABLE trade_code_hydrographics (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    trade_code_id INTEGER NOT NULL,
    hydrographics_id INTEGER NOT NULL,
    FOREIGN KEY (trade_code_id) REFERENCES trade_code_type(id),
    FOREIGN KEY (hydrographics_id) REFERENCES hydrographics_type(id)
);

CREATE TABLE trade_code_population (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    trade_code_id INTEGER NOT NULL,
    population_id INTEGER NOT NULL,
    FOREIGN KEY (trade_code_id) REFERENCES trade_code_type(id),
    FOREIGN KEY (population_id) REFERENCES population_type(id)
);

CREATE TABLE trade_code_government (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    trade_code_id INTEGER NOT NULL,
    government_id INTEGER NOT NULL,
    FOREIGN KEY (trade_code_id) REFERENCES trade_code_type(id),
    FOREIGN KEY (government_id) REFERENCES government_type(id)
);

CREATE TABLE trade_code_law_level (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    trade_code_id INTEGER NOT NULL,
    law_level_id INTEGER NOT NULL,
    FOREIGN KEY (trade_code_id) REFERENCES trade_code_type(id),
    FOREIGN KEY (law_level_id) REFERENCES law_level_type(id)
);

CREATE TABLE trade_code_tech_level (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    trade_code_id INTEGER NOT NULL,
    tech_level_id INTEGER NOT NULL,
    FOREIGN KEY (trade_code_id) REFERENCES trade_code_type(id),
    FOREIGN KEY (tech_level_id) REFERENCES tech_level_type(id)
);