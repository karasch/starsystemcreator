import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';
import {Button, Form, FormGroup, FormControl, FormLabel, FormCheck} from 'react-bootstrap';
import axios from 'axios';


function TradeCodes(props){
  const codes = props.codes;
//TODO mouseover explanations?
  return(
    codes.map((codeType) => codeType.code).join("")
  );
  
}

function Faction(props){
  const faction = props.faction;
  return(
    <ul>{faction.id}
      <li>Strength: {faction.factionType?.relativeStrength}</li>
      <li>Type: {faction.governmentType?.name}</li>
      <li>Description: {faction.governmentType?.description}</li>
    </ul>
  );
}

function FactionForm(props){

  const faction = props.fctn;
  const governmentList = props.govList;
  const strengthList = props.strList;
  const handleGovernmentChange = props.govChangeFunc;
  const factionKey = props.factionKey;
  const handleStrengthChange = props.strChangeFunc;

  return(
    <FormGroup>
      <div>{faction.id}</div>
      <FormLabel>Government Type</FormLabel>
      <FormControl as="select" onChange={e => handleGovernmentChange(factionKey, e.target.value)} defaultValue={faction.governmentType.id}>
        { governmentList.map((gov) => <option value={gov.id}>{gov.name}</option>) }
      </FormControl>
      <FormLabel>Faction Strength</FormLabel>
      <FormControl as="select" onChange={e => handleStrengthChange(factionKey, e.target.value)} defaultValue={faction.factionType.id}>
        { strengthList.map( (str) => <option value={str.id}>{str.relativeStrength}</option> )}
      </FormControl>
    </FormGroup>
  );
}

class UpdatePlanetForm extends Component{
  constructor(props){
    super(props);

    this.state = {
      updatedPlanetData: props.data,
      sizeList: props.sizes,
      atmosphereList: props.atmos,
      temperatureList: props.temps,
      hydrographicsList: props.hydroList,
      populationList: props.popList,
      governmentList: props.govList,
      factionStrengthList: props.facList,
      starPortTypeList: props.portList,
      techLevelList: props.techList,
      travelZoneList: props.tzList,
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSizeChange = this.handleSizeChange.bind(this);
    this.handleAtmosphereChange = this.handleAtmosphereChange.bind(this);
    this.handleTemperatureChange = this.handleTemperatureChange.bind(this);
    this.handleHydroChange = this.handleHydroChange.bind(this);
    this.handlePopulationChange = this.handlePopulationChange.bind(this);
    this.handleGovernmentChange = this.handleGovernmentChange.bind(this);
    this.handleFactionGovernmentChange = this.handleFactionGovernmentChange.bind(this);
    this.handleFactionStrengthChange = this.handleFactionStrengthChange.bind(this);
    this.handleStarPortTypeChange = this.handleStarPortTypeChange.bind(this);
    this.handleBerthingChange = this.handleBerthingChange.bind(this);
    this.handleNavalBaseCheck = this.handleNavalBaseCheck.bind(this);
    this.handleScoutBaseCheck = this.handleScoutBaseCheck.bind(this);
    this.handleResearchBaseCheck = this.handleResearchBaseCheck.bind(this);
    this.handleTravellersAidCheck = this.handleTravellersAidCheck.bind(this);
    this.handleTechLevelChange = this.handleTechLevelChange.bind(this);
    this.handleTravelZoneChange = this.handleTravelZoneChange.bind(this);
  }

  handleNameChange(event){
    if(event != null){
      const name = event.target.value.trim();
      this.setState(prevState => ({
        // Set updatedPlanetData
        updatedPlanetData: {
          // to a copy of what was in it
          ...prevState.updatedPlanetData,
          // then update the name you as needed.
          name: name,
        }
      }));

    }
  }

  handleSizeChange(event){
    if(event != null){
      const sizeId = event.target.value;
      this.setState(prevState => ({
        updatedPlanetData: {
          ...prevState.updatedPlanetData,
          size: {id: sizeId}, 
        }
      }));
    }
  }

  handleAtmosphereChange(event){
    if(event != null){
      const atmosphereId = event.target.value;
      this.setState(prevState => ({
        updatedPlanetData: {
          ...prevState.updatedPlanetData,
          atmosphere: {id: atmosphereId},
        }
      }));
    }
  }

  handleTemperatureChange(event){
    if(event != null){
      const tempId = event.target.value;
      this.setState(prevState => ({
        updatedPlanetData: {
          ...prevState.updatedPlanetData,
          temperature: {id: tempId},
        }
      }));
    }
  }

  handleHydroChange(event){
    if(event != null){
      const hydroId = event.target.value;
      this.setState(prevState => ({
        updatedPlanetData: {
          ...prevState.updatedPlanetData,
          hydrographicsType: {id: hydroId},
        }
      }));
    }
  }

  handlePopulationChange(event){
    if(event != null){
      const pop = event.target.value;
      this.setState(prevState => ({
        updatedPlanetData: {
          ...prevState.updatedPlanetData,
          population: {id: pop},
        }
      }));
    }
  }
  
  handleGovernmentChange(event){
    if(event != null){
      const gov = event.target.value;
      this.setState(prevState => ({
        updatedPlanetData: {
          ...prevState.updatedPlanetData,
          governmentType: {id: gov},
        }
      }));
    }    
  }

  handleFactionGovernmentChange(factionId, govId){
    let planetCopy = { ...this.state.updatedPlanetData };
    planetCopy.factions[factionId].governmentType.id = govId;
    this.setState({updatedPlanetData: planetCopy});
  }

  handleFactionStrengthChange(factionId, strId){
    let planetCopy = { ...this.state.updatedPlanetData };
    planetCopy.factions[factionId].factionType.id = strId;
    this.setState({updatedPlanetData: planetCopy});
  }

  handleStarPortTypeChange(event){
    let planetCopy = { ...this.state.updatedPlanetData };
    planetCopy.starPort.starPortType.id = event.target.value;
    this.setState({updatedPlanetData: planetCopy});
  }

  handleBerthingChange(event){
    let planetCopy = { ...this.state.updatedPlanetData };
    planetCopy.starPort.berthingCost = event.target.value;
    this.setState({updatedPlanetData: planetCopy});
  }

  handleNavalBaseCheck(event){
    let planetCopy = { ...this.state.updatedPlanetData };
    planetCopy.starPort.navalBase = event.target.checked;
    this.setState({updatedPlanetData: planetCopy});
  }

  handleScoutBaseCheck(event){
    let planetCopy = { ...this.state.updatedPlanetData };
    planetCopy.starPort.scoutBase = event.target.checked;
    this.setState({updatedPlanetData: planetCopy});
  }

  handleResearchBaseCheck(event){
    let planetCopy = { ...this.state.updatedPlanetData };
    planetCopy.starPort.researchBase = event.target.checked;
    this.setState({updatedPlanetData: planetCopy});
  }

  handleTravellersAidCheck(event){
    let planetCopy = { ...this.state.updatedPlanetData };
    planetCopy.starPort.travellersAidSociety = event.target.checked;
    this.setState({updatedPlanetData: planetCopy});
  }

  handleTechLevelChange(event){
    if(event != null){
      const tl = event.target.value;
      this.setState(prevState => ({
        updatedPlanetData: {
          ...prevState.updatedPlanetData,
          techLevel: {id: tl},
        }
      }));
    }    
  }

  handleTravelZoneChange(event){
    if(event != null){
      const tz = event.target.value;
      this.setState(prevState => ({
        updatedPlanetData: {
          ...prevState.updatedPlanetData,
          travelZone: {id: tz},
        }
      }));
    }   
  }

  render(){

    return(
      <div>
        <Form>
          <FormGroup>
            <FormLabel>Id</FormLabel>
            <FormControl type="text" defaultValue={this.state.updatedPlanetData.id} readOnly />
          </FormGroup>
          <FormGroup>
            <FormLabel>Name</FormLabel>
            <FormControl type="text" defaultValue={this.state.updatedPlanetData.name} onChange={this.handleNameChange}/>
          </FormGroup>
          <FormGroup>
            <FormLabel>Size</FormLabel>
            <FormControl as="select" value={this.state.updatedPlanetData.size.id} onChange={this.handleSizeChange}>
              { this.state.sizeList.map((st) =>  <option value={st.id} >{st.diameter}</option> ) }
            </FormControl>
          </FormGroup>
          <FormGroup>
            <FormLabel>Atmosphere</FormLabel>
            <FormControl as="select" value={this.state.updatedPlanetData.atmosphere.id} onChange={this.handleAtmosphereChange}>
              { this.state.atmosphereList.map( (atmo) => <option value={atmo.id}>{atmo.composition} {atmo.pressure}</option>)}
            </FormControl>
          </FormGroup>
          <FormGroup>
            <FormLabel>Temperature</FormLabel>
            <FormControl as="select" value={this.state.updatedPlanetData.temperature.id} onChange={this.handleTemperatureChange}>
              { this.state.temperatureList.map( (temp) => <option value={temp.id}>{temp.type} ({temp.description})</option> )}
            </FormControl>
          </FormGroup>
          <FormGroup>
            <FormLabel>Water</FormLabel>
            <FormControl as="select" value={this.state.updatedPlanetData.hydrographicsType.id} onChange={this.handleHydroChange}>
              { this.state.hydrographicsList.map( (hydro) => <option value={hydro.id}>{hydro.hydrographicPercentage}{hydro.description}</option> )}
            </FormControl>
          </FormGroup>
          <FormGroup>
            <FormLabel>Population</FormLabel>
            <FormControl as="select" value={this.state.updatedPlanetData.population.id} onChange={this.handlePopulationChange}>
              { this.state.populationList.map( (pop) => <option value={pop.id}>{pop.inhabitants}</option>)}
            </FormControl>
          </FormGroup>
          <FormGroup>
            <FormLabel>Government</FormLabel>
            <FormControl as="select" value={this.state.updatedPlanetData.governmentType.id} onChange={this.handleGovernmentChange}>
              { this.state.governmentList.map( (gov) => <option value={gov.id}>{gov.name}</option>)}
            </FormControl>
          </FormGroup>
          { this.state.updatedPlanetData.factions.map( (fac, index) => <FactionForm fctn={fac} govList={this.state.governmentList} factionKey={index} govChangeFunc={this.handleFactionGovernmentChange} strList={this.state.factionStrengthList}  strChangeFunc={this.handleFactionStrengthChange} /> ) }
          <FormGroup>
            <FormLabel>Star Port</FormLabel>
            <FormControl as="select" defaultValue={this.state.updatedPlanetData.starPort.starPortType.id} onChange={this.handleStarPortTypeChange}>
              { this.state.starPortTypeList.map( (portType) => <option value={portType.id}>{portType.starPortClass} - {portType.quality}</option>)}
            </FormControl>
          </FormGroup>
          <FormGroup>
            <FormLabel>Berthing Cost</FormLabel>
            <FormControl type="text" defaultValue={this.state.updatedPlanetData.starPort.berthingCost} onChange={this.handleBerthingChange} />
          </FormGroup>
          <FormGroup>
            <FormLabel>Bases</FormLabel>
            <FormCheck type="checkbox" checked={this.state.updatedPlanetData.starPort.navalBase} label="Naval Base" onChange={this.handleNavalBaseCheck} />
            <FormCheck type="checkbox" checked={this.state.updatedPlanetData.starPort.scoutBase} label="Scout Base" onChange={this.handleScoutBaseCheck} />
            <FormCheck type="checkbox" checked={this.state.updatedPlanetData.starPort.researchBase} label="Research Base" onChange={this.handleResearchBaseCheck} />
            <FormCheck type="checkbox" checked={this.state.updatedPlanetData.starPort.travellersAidSociety} label="Travellers Aid Society" onChange={this.handleTravellersAidCheck} />
          </FormGroup>
          <FormGroup>
            <FormLabel>Tech Level</FormLabel>
            <FormControl as="select" defaultValue={this.state.updatedPlanetData.techLevel.id} onChange={this.handleTechLevelChange}>
              { this.state.techLevelList.map( (tech) => <option value={tech.id}>{tech.score}</option>)}
            </FormControl>
          </FormGroup>
          <FormGroup>
            <FormLabel>Travel Zone</FormLabel>
            <FormControl as="select" defaultValue={this.state.updatedPlanetData.travelZone?.id} onChange={this.handleTravelZoneChange}>
              { this.state.travelZoneList.map( (zone) => <option value={zone.id}>{zone.name}</option>) }
            </FormControl>
          </FormGroup>
          <Button onClick={() => this.props.buttonClick(this.state.updatedPlanetData)}>Save Update</Button>
        </Form>
      </div>
    );
  }
}


class Planet extends Component {


  render(){

    const planetData = this.props.data;

    if(planetData == null){
      return(
        <div>Waiting...</div>
      );
    }
    return(
      <div>
        <ul>
          <li>Id: {planetData.id}</li>
          <li>Name: {planetData.name}</li>
          <li>Size: 
            <ul>
              <li>Diameter: {planetData.size?.diameter} km</li>
              <li>Example: {planetData.size?.example}</li>
            </ul>
          </li>
          <li>Atmosphere:
            <ul>
              <li>Composition: {planetData.atmosphere?.composition}</li>
              <li>Pressure: {planetData.atmosphere?.pressure}</li>
            </ul>
          </li>
          <li>Temperature:
            <ul>
              <li>Average Temp: {planetData.temperature?.averageTemperature} ({planetData.temperature?.type})</li>
            </ul>
          </li>
          <li>Water:
            <ul>
              <li>Surface Water: {planetData.hydrographicsType?.hydrographicPercentage} ({planetData.hydrographicsType?.description})</li>
            </ul>
          </li>
          <li>Population: 
            <ul>
              <li>{planetData.population?.inhabitants} (e.g. {planetData.population?.description})</li>
            </ul>
          </li>
          <li>Government:
            <ul>
              <li>Type: {planetData.governmentType?.name}</li>
              <li>Description: {planetData.governmentType?.description}</li>
            </ul>
          </li>
          <li>Rival Factions:
            <ul>
              { planetData.factions.map((fctn) => <Faction faction={fctn}/>) }
            </ul>
          </li>
          <li>Law:
            <ul>
              <li>Banned Weapons: {planetData.lawLevel?.weaponsBanned}</li>
              <li>Banned Armor: {planetData.lawLevel?.armorBanned}</li>
              <li>Common Contraband: {planetData.governmentType?.commonContraband}</li>
            </ul>
          </li>
          <li>Star Port:
            <ul>
              <li>Class: {planetData.starPort?.starPortType.class} ({planetData.starPort?.starPortType.quality}) </li>
              <li>Fuel Available: {planetData.starPort?.starPortType.fuel} </li>
              <li>Facilities: {planetData.starPort?.starPortType.facilities}</li>
              <li>Berthing Cost: {planetData.starPort?.berthingCost}</li>
              {/* TODO: the Base?.toString() is a quick and dirty fix.  null should display false. */}
              <li>Naval Base: {planetData.starPort?.navalBase?.toString()}</li>
              <li>Scout Base: {planetData.starPort?.scoutBase?.toString()}</li>
              <li>Research Base: {planetData.starPort?.researchBase?.toString()}</li>
              <li>Travellers' Aid Society: {planetData.starPort?.travellersAidSociety?.toString()}</li>
            </ul>
          </li>
          <li>Travel Information:
            <ul>
              <li>Tech Level: {planetData.techLevel?.score}</li>
              <li>Travel Advisory: {planetData.travelZone?.name}</li>
              <li>Trade Codes: <TradeCodes codes={planetData.tradeCodes} /></li>
            </ul>
          </li>
        </ul>
      </div>
    );
  }
}

var fakeData = {"id":3,"name":null,"size":{"id":1,"diameter":"1,600","example":"Triton","surfaceGravity":"0.05"},"atmosphere":{"id":0,"composition":"None","pressure":"0.00","survivalGearRequired":"Vacc Suit"},"temperature":{"id":2,"type":"Cold","averageTemperature":"-50 to 0","description":"Icy world.  Little liquid water, extensive ice caps, few clouds."},"hydrographicsType":{"id":0,"hydrographicPercentage":"0-5%","description":"Desert World"},"population":{"id":2,"inhabitants":"Hundreds","range":"100+","description":"A village"},"governmentType":{"id":1,"name":"Company/Corporation","description":"Ruling functions are assumed by a company managerial elite, and most citizenry are company employees or dependants","examples":"Corporate outpost, asteroid mine, feudal domain","commonContraband":"Weapons, Drugs, Travellers"},"factions":[],"lawLevel":{"id":1,"weaponsBanned":"Poison gas, explosives, undetectable weapons, WMD","armorBanned":"Battle dress"},"starPort":{"id":2,"starPortType":{"id":6,"starPortClass":"X","quality":"No starport","berthingCost":"0","fuel":"None","facilities":"None","bases":"None"},"berthingCost":0,"navalBase":null,"scoutBase":null,"researchBase":null,"travellersAidSociety":null},"techLevel":{"id":6,"score":"6"},"travelZone":null,"tradeCodes":[{"id":12,"code":"Lt","classification":"Low Tech"},{"id":9,"code":"Ic","classification":"Ice Capped"},{"id":11,"code":"Lo","classification":"Low Population"},{"id":14,"code":"NI","classification":"Non-Industrial"},{"id":17,"code":"Va","classification":"Vacuum"}]};
const restURL = ""; //"http://localhost:8080";

class App extends Component {
  constructor(){
    super();
    this.state = {
      data: null,
      planetIdField: 1,
      planetList: [],
      mode: "",
      sizeList: [],
      atmoList: [],
      tempList: [],
      hydroList: [],
      populationList: [],
      governmentList: [],
      factionStrengthList: [],
      starPortList: [],
      techLevelList: [],
      travelZoneList: [],
    }

    this.handleIdChange = this.handleIdChange.bind(this);
    this.viewSpecificPlanet = this.viewSpecificPlanet.bind(this);
    this.createRandomPlanet = this.createRandomPlanet.bind(this);
    this.getPlanetList = this.getPlanetList.bind(this);
    this.getSizeList = this.getSizeList.bind(this);
    this.getAtmosphereList = this.getAtmosphereList.bind(this);
    this.getTemperatureList = this.getTemperatureList.bind(this);
    this.getHydrographicsList = this.getHydrographicsList.bind(this);
    this.getPopulationList = this.getPopulationList.bind(this);
    this.getGovernmentList = this.getGovernmentList.bind(this);
    this.getFactionStrengths = this.getFactionStrengths.bind(this);
    this.getStarPortTypes = this.getStarPortTypes.bind(this);
    this.getTechLevels = this.getTechLevels.bind(this);
    this.getTravelZones = this.getTravelZones.bind(this);
    this.showPlanetUpdateForm = this.showPlanetUpdateForm.bind(this);
    //this.getPlanetData = this.getPlanetData.bind(this);
    this.updatePlanet = this.updatePlanet.bind(this);

    this.getPlanetList();
    this.getSizeList();
    this.getAtmosphereList();
    this.getTemperatureList();
    this.getHydrographicsList();
    this.getPopulationList();
    this.getGovernmentList();
    this.getFactionStrengths();
    this.getStarPortTypes();
    this.getTechLevels();
    this.getTravelZones();
  }


  handleIdChange(event){
    if(event.target.value != null){
      const id = event.target.value;
      this.setState({planetIdField: id});
    }
    
  }

  viewSpecificPlanet(){
    this.setState({mode: ""});
    axios.get(restURL + "/planet/" + this.state.planetIdField).then((response) => { const planetData = response.data; this.setState({data: planetData, mode: "view"}); });
  }

  showPlanetUpdateForm(){
    this.setState({mode: ""});
    axios.get(restURL + "/planet/" + this.state.planetIdField).then((response) => { const planetData = response.data; this.setState({data: planetData, mode: "update"}); });
  }

  //getPlanetData(){
  //  console.log("Current ID is " + this.state.planetIdField);
  //  console.log("Getting " + "http://localhost:8080/planet/" + this.state.planetIdField)
  //  return axios.get("http://localhost:8080/planet/" + this.state.planetIdField).then((response) => { const planetData = response.data; this.setState({data: planetData}); });
  //}

  createRandomPlanet(){
    axios.get(restURL + "/random").then((response) => {this.setState({data: response.data}); this.getPlanetList();});
    //this.setState({data: fakeData});
    
  }

  updatePlanet(updatedData){
    //send the updated data to the rest endpoint.
    axios.post(restURL + "/update", updatedData).then((response) => {console.log("response received"); console.log(response); this.getPlanetList();}).catch((response) => console.log(response));
  }

  getPlanetList(){
    console.log("Updating planet list");
    axios.get(restURL + "/list").then((response) => this.setState({planetList: response.data}));
    console.log("Update complete.");
  }

  getSizeList(){
    axios.get(restURL + "/sizes").then((response) => this.setState({sizeList: response.data}));
  }

  getAtmosphereList(){
    axios.get(restURL + "/atmospheres").then((response) => this.setState({atmoList: response.data}));
  }

  getTemperatureList(){
    axios.get(restURL + "/temperatures").then((response) => this.setState({tempList: response.data}));
  }

  getHydrographicsList(){
    axios.get(restURL + "/hydrographics").then((response) => this.setState({hydroList: response.data}));
  }

  getPopulationList(){
    axios.get(restURL + "/populations").then((response) => this.setState({populationList: response.data}));
  }

  getGovernmentList(){
    axios.get(restURL + "/governments").then((response) => this.setState({governmentList: response.data}));
  }

  getFactionStrengths(){
    axios.get(restURL + "/factionstrengths").then((response) => this.setState({factionStrengthList: response.data}));
  }

  getStarPortTypes(){
    axios.get(restURL + "/starporttypes").then((response) => this.setState({starPortList: response.data}));
  }

  getTechLevels(){
    axios.get(restURL + "/techlevels").then((response) => this.setState({techLevelList: response.data}));
  }

  getTravelZones(){
    axios.get(restURL + "/travelzones").then((response) => this.setState({travelZoneList: response.data}));
  }
  

  render(){

    //console.log("Host is " + window.location.host);
    //console.log("PlanetList is " + this.state.planetList);

    //const listItems = this.state.planetList.map((planet) => <option value={planet.id}>{planet.id} {planet.name}</option>)

    let modeForm;
    if(this.state.mode === "update"){
      modeForm = <UpdatePlanetForm data={this.state.data} buttonClick={this.updatePlanet} sizes={this.state.sizeList} 
                    atmos={this.state.atmoList} temps={this.state.tempList} hydroList={this.state.hydroList}
                    popList={this.state.populationList} govList={this.state.governmentList} facList={this.state.factionStrengthList}
                    portList={this.state.starPortList} techList={this.state.techLevelList} tzList={this.state.travelZoneList} />
    }
    else if (this.state.mode === "view"){
      modeForm = <Planet data={this.state.data}/>
    }
    else {
      modeForm = <div>Waiting...</div>
    }
    
    return(
      <div>
        <h1>Planets!</h1>
        <Form>
          <Button class="btn btn-primary" onClick={this.createRandomPlanet}>Generate Random Planet</Button>
          <FormGroup name="planetIdField">
            <FormLabel >Planet Id</FormLabel>
            {/*<!-- <FormControl type="text" onChange={this.handleIdChange}></FormControl> -->*/}
            <FormControl as="select" onChange={this.handleIdChange}>
              { this.state.planetList.map((planet) => <option value={planet.id}>{planet.id} {planet.name}</option>) }
            </FormControl>
          </FormGroup>
          <Button class="btn btn-primary" onClick={this.viewSpecificPlanet}>View planet</Button>
          <Button class="btn btn-primary" onClick={this.showPlanetUpdateForm}>Edit Planet</Button>
        </Form>
        {modeForm}
        
        
      </div>
    );
  }
}

export default App;
