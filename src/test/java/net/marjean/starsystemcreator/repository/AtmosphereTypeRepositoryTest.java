package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.AtmosphereType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class AtmosphereTypeRepositoryTest {

    @Autowired
    private AtmosphereTypeRepository testUnit;

    @Test
    public void testFindAll(){

        int count = 0;
        for(AtmosphereType at : testUnit.findAll()){
            count++;
        }

        assertEquals(16, count);

    }

    @Test
    public void testFindById(){
        AtmosphereType at = testUnit.findById(1).get();

        assertNotNull(at);
        assertEquals("Trace", at.getComposition());
    }
}
