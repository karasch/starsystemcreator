package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.Faction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class FactionRepositoryTest {

    @Autowired
    private FactionRepository testUnit;

    @Test
    public void testFindById(){
        Faction f = testUnit.findById(1).get();

        assertNotNull(f);
        assertEquals(1, f.getGovernmentType().getId());
    }
}
