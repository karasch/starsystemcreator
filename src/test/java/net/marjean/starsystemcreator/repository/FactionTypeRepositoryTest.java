package net.marjean.starsystemcreator.repository;

import net.marjean.starsystemcreator.model.FactionType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

//@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class FactionTypeRepositoryTest {

    @Autowired
    private FactionTypeRepository testUnit;

    @Test
    public void testFindFactionByRoll(){

        System.out.println(testUnit.findFactionByRoll(12).toString());

        FactionType faction = testUnit.findFactionByRoll(12);
        assertNotNull(faction);
        assertEquals(6, faction.getId());
    }

    @Test
    public void testFindById(){
        FactionType ft = testUnit.findById(1).get();

        assertEquals(2, ft.getMinRoll());
        assertEquals(3, ft.getMaxRoll());
    }
}
