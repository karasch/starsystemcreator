package net.marjean.starsystemcreator.service;

import net.marjean.starsystemcreator.model.Planet;
import net.marjean.starsystemcreator.model.StarPort;
import net.marjean.starsystemcreator.model.StarPortType;
import net.marjean.starsystemcreator.repository.*;
import net.marjean.starsystemcreator.util.DieRoller;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlanetCreatorTest {

    private static PlanetCreator testUnit;
    private static DieRoller dice;
    private static SizeTypeRepository sizeTypeRepository;
    private static AtmosphereTypeRepository atmosphereTypeRepository;
    private static TemperatureTypeRepository temperatureTypeRepository;
    private static HydrographicsTypeRepository hydrographicsTypeRepository;
    private static PopulationTypeRepository populationTypeRepository;
    private static GovernmentTypeRepository governmentTypeRepository;
    private static FactionTypeRepository factionTypeRepository;
    private static LawLevelTypeRepository lawLevelTypeRepository;
    private static StarPortTypeRepository starPortTypeRepository;
    private static TechLevelTypeRepository techLevelTypeRepository;
    private static TravelCodeTypeRepository travelZoneTypeRepository;
    private static TradeCodeTypeRepository tradeCodeTypeRepository;

    /**
     * Set up mock objects and the PlanetCreator instance for testing
     */
    @BeforeAll
    public static void setUp(){
        dice = Mockito.mock(DieRoller.class);
        sizeTypeRepository = Mockito.mock(SizeTypeRepository.class);
        atmosphereTypeRepository = Mockito.mock(AtmosphereTypeRepository.class);
        temperatureTypeRepository = Mockito.mock(TemperatureTypeRepository.class);
        hydrographicsTypeRepository = Mockito.mock(HydrographicsTypeRepository.class);
        populationTypeRepository = Mockito.mock(PopulationTypeRepository.class);
        governmentTypeRepository = Mockito.mock(GovernmentTypeRepository.class);
        factionTypeRepository = Mockito.mock(FactionTypeRepository.class);
        lawLevelTypeRepository = Mockito.mock(LawLevelTypeRepository.class);
        starPortTypeRepository = Mockito.mock(StarPortTypeRepository.class);
        techLevelTypeRepository = Mockito.mock(TechLevelTypeRepository.class);
        travelZoneTypeRepository = Mockito.mock(TravelCodeTypeRepository.class);
        tradeCodeTypeRepository = Mockito.mock(TradeCodeTypeRepository.class);

        testUnit = new PlanetCreator(dice, sizeTypeRepository, atmosphereTypeRepository, temperatureTypeRepository,
                hydrographicsTypeRepository, populationTypeRepository, governmentTypeRepository,
                factionTypeRepository, lawLevelTypeRepository, starPortTypeRepository, techLevelTypeRepository,
                travelZoneTypeRepository, tradeCodeTypeRepository);
    }

    /**
     * Reset all the mocked objects after each test
     */
    @AfterEach
    public void cleanUp(){
        Mockito.reset(dice, sizeTypeRepository, atmosphereTypeRepository, temperatureTypeRepository,
                hydrographicsTypeRepository, populationTypeRepository, governmentTypeRepository,
                factionTypeRepository, lawLevelTypeRepository, starPortTypeRepository, techLevelTypeRepository,
                travelZoneTypeRepository, tradeCodeTypeRepository);
    }

    @Test
    public void testGetTechLevel_0(){

        Mockito.when(dice.rollD6()).thenReturn(1);

        StarPortType spt = new StarPortType();
        spt.setId(1);
        StarPort sp = new StarPort();
        sp.setStarPortType(spt);

        int retVal = testUnit.getTechLevel(0,0,0,0,0, sp);

        Mockito.verify(dice).rollD6();
        assertEquals(12, retVal);
    }

    @Test
    public void testGetTechLevel_1(){
        Mockito.when(dice.rollD6()).thenReturn(1);

        StarPortType spt = new StarPortType();
        spt.setId(2);
        StarPort sp = new StarPort();
        sp.setStarPortType(spt);

        int retVal = testUnit.getTechLevel(1,1, 1, 1, 1, sp);

        Mockito.verify(dice).rollD6();
        assertEquals(9, retVal);
    }

    @Test
    public void testGetStarPortScore_0(){

        Mockito.when(dice.roll2d6()).thenReturn(2);

        int retVal = testUnit.getStarportScore(0);

        Mockito.verify(dice).roll2d6();
        assertEquals(2, retVal);

    }

    @Test
    public void testGetStarPortScore_10(){
        Mockito.when(dice.roll2d6()).thenReturn(2);

        int retVal = testUnit.getStarportScore(10);

        Mockito.verify(dice).roll2d6();
        assertEquals(4, retVal);
    }

    @Test
    public void testGetStarPortScore_high(){
        //StarPortScore should never be above 11
        Mockito.when(dice.roll2d6()).thenReturn(12);

        int retVal = testUnit.getStarportScore(10);

        Mockito.verify(dice).roll2d6();
        assertEquals(11, retVal);
    }

    @Test
    public void testGetLawLevel_low(){
        // roll for law level: 2d6 - 7 + government stat
        // min 0, max 9

        Mockito.when(dice.roll2d6()).thenReturn(2);

        int retVal = testUnit.getLawLevel(1);

        Mockito.verify(dice).roll2d6();
        assertEquals(0, retVal);
    }

    @Test
    public void testGetLawLevel_high(){
        Mockito.when(dice.roll2d6()).thenReturn(12);

        int retVal = testUnit.getLawLevel(11);

        Mockito.verify(dice).roll2d6();
        assertEquals(9, retVal);
    }

    @Test
    public void testGetLawLevel_8(){
        Mockito.when(dice.roll2d6()).thenReturn(12);

        int retVal = testUnit.getLawLevel(3);

        Mockito.verify(dice).roll2d6();
        assertEquals(8, retVal);
    }

    @Test
    public void testGetGovernment(){
        Mockito.when(dice.roll2d6()).thenReturn(6);

        int retVal = testUnit.getGovernment(7);

        Mockito.verify(dice).roll2d6();
        assertEquals(6, retVal);
    }

    @Test
    public void testGetGovernment_low(){

        Mockito.when(dice.roll2d6()).thenReturn(2);

        int retVal = testUnit.getGovernment(1);

        Mockito.verify(dice).roll2d6();
        assertEquals(0, retVal);
    }

    @Test
    public void testGetGovernment_high(){

        Mockito.when(dice.roll2d6()).thenReturn(12);

        int retVal = testUnit.getGovernment(15);

        Mockito.verify(dice).roll2d6();
        assertEquals(15, retVal);
    }

    @Test
    public void testGetGovernment_pop0(){
        int retVal = testUnit.getGovernment(0);

        Mockito.verifyNoInteractions(dice);
        assertEquals(0, retVal);
    }

    @Test
    public void testGetPopulation(){
        Mockito.when(dice.roll2d6()).thenReturn(12);

        int retVal = testUnit.getPopulation();

        Mockito.verify(dice).roll2d6();
        assertEquals(10, retVal);
    }

    @Test
    public void testGetPlanetSize(){
        Mockito.when(dice.roll2d6()).thenReturn(12);

        int retVal = testUnit.getPlanetSize();

        Mockito.verify(dice).roll2d6();
        assertEquals(10, retVal);
    }

    @Test
    public void testGetStarPort(){

        StarPortType spt = new StarPortType();
        spt.setId(57);
        spt.setBerthCostMultiplier(1);
        spt.setNavalBaseRoll(10);
        spt.setScoutBaseRoll(10);
        spt.setResearchBaseRoll(10);
        spt.setTasRoll(10);
        Mockito.when(starPortTypeRepository.findStarPortByRoll(12)).thenReturn(Optional.of(spt));

        Planet p = new Planet();
        p.setId(23);

        // set rolls for bases & travellers aid society
        Mockito.when(dice.roll2d6()).thenReturn(12);

        // set roll for berthing cost
        Mockito.when(dice.rollD6()).thenReturn(6);

        StarPort retVal = testUnit.getStarPort(p, 12);

        assertEquals(spt, retVal.getStarPortType());
        assertEquals(p, retVal.getPlanet());
        assertEquals(6, retVal.getBerthingCost());
        assert(retVal.getNavalBase());
        assert(retVal.getResearchBase());
        assert(retVal.getScoutBase());
        assert(retVal.getTravellersAidSociety());

    }

    //TODO test createRandom()
}
